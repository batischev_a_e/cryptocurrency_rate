import { getConfig } from "../db/sequelize.config";
import TradeModel from "../db/trade.model";

import { Service, ServiceBroker, Context } from "moleculer";
import { Sequelize } from 'sequelize';

interface DbServiceSettings {
    sequelize: Sequelize;
}

class DbService extends Service {
    public settings: DbServiceSettings;

    constructor(broker: ServiceBroker) {
        super(broker);

        const sequelize = getConfig();
        sequelize.define('Trade', TradeModel(sequelize));

        this.settings = {
            sequelize
        };

        this.parseServiceSchema({
            name: "budget",
            mixins: [DbMixin],
            actions: {
                create: {
                    params: {
                        mts: { type: "number" },
                        amount: { type: "number" },
                        price: { type: "number" }
                    },
                    handler(ctx: Context<{ mts: number; amount: number; price: number }>) {
                        return this.createTrade(ctx.params);
                    }
                }
            },
            methods: {
                createTrade(data: { mts: number; amount: number; price: number }) {
                    return this.settings.sequelize.model('Trade').create(data);
                }
            },
            started: async () => {
                await sequelize.authenticate();
                console.log('DB connection has been established successfully.');
            }
        });
    }
}

const DbMixin = {};