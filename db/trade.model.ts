import { DataTypes } from 'sequelize';
import { getConfig } from '../db/sequelize.config'; 

const sequelize = getConfig();

const TradeModel = sequelize.define('Trade', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    mts: {
        type: DataTypes.BIGINT
    },
    amount: {
        type: DataTypes.FLOAT
    },
    price: {
        type: DataTypes.FLOAT
    }
}, {
    tableName: 'trades',
    timestamps: false
});

export default TradeModel;