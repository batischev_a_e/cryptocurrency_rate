import { Service, ServiceBroker } from "moleculer";
import TradeModel  from "../db/trade.model";

export default class TradeService extends Service {
    public constructor(broker: ServiceBroker) {
        super(broker);

        this.parseServiceSchema({
            name: "trades",
            actions: {
                getTrades: {
                    rest: "GET /",
                    async handler() {
                        return TradeModel.findAll({
                            attributes: ["id", "mts", "amount", "price"]
                        });
                    },
                },
            }
        });
    }
}
