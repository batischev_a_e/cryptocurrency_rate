import { config } from 'dotenv';
import * as path from 'path';
import { Sequelize } from 'sequelize';

export function getConfig() {
    const envPath = path.join(__dirname, '../.env');
    config({ path: envPath });
    
    const dbConfig = {
        dialect: 'postgres' as const,  
        migrationStorageTableName: 'migrations_sequelize',
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT || '5432'),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        logging: console.log
    };

    return new Sequelize(dbConfig);
}
