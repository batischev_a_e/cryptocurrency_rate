import { Service, ServiceBroker } from "moleculer";
import fetch from 'node-fetch';
import TradeModel from "../db/trade.model";

export default class WorkerService extends Service {
    private intervalId: NodeJS.Timeout | null = null;

    public constructor(broker: ServiceBroker) {
        super(broker);

        this.parseServiceSchema({
            name: "worker",
            actions: {
                startFetchingTrades: {
                    handler: async () => {
                        if (!this.intervalId) {
                            this.intervalId = setInterval(async () => {
                                await this.getAndSaveTrades();
                            }, 30000);
                        }
                    }
                },

                stopFetchingTrades: {
                    handler: async () => {
                        if (this.intervalId) {
                            clearInterval(this.intervalId);
                            this.intervalId = null;
                        }
                    }
                }
            },
            started: async () => {
                await this.actions.startFetchingTrades.call(this);
            }
        });
    }

    private async getAndSaveTrades(): Promise<void> {
        try {
            const response = await fetch('https://api-pub.bitfinex.com/v2/trades/fUSD/hist?limit=15');
            
            if (!response.ok) {
                throw new Error('Error fetching data: ' + response.statusText);
            }

            const trades = await response.json() as [number, number, number, number][];
            const newData = trades.map(trade => ({
                id: trade[0],
                mts: trade[1],
                amount: trade[2],
                price: trade[3]
            }));

            await TradeModel.bulkCreate(newData, {
                updateOnDuplicate: ["mts", "amount", "price"]
            });

        } catch (error) {
            console.error('Error saving or fetching trades:', error);
        }
    }
}
