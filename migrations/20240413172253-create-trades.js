'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('trades', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      mts: {
        type: Sequelize.BIGINT
      },
      amount: {
        type: Sequelize.FLOAT
      },
      price: {
        type: Sequelize.FLOAT
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('trades');
  }
};
